import React from 'react';
import { View } from 'react-native';
import { Header } from 'react-native-elements'
import AlbumList from './src/components/AlbumList';

const App = () => (
  <View>
    <Header
      leftComponent={{ icon: 'menu', color: '#fff' }}
      centerComponent={{ text: 'Albums', style: { color: '#fff' } }}
      rightComponent={{ icon: 'home', color: '#fff' }}
    />    
    <AlbumList />
  </View>
);

export default App

