import React from 'react';
import { Text, View, Image, Linking } from 'react-native';
// https://facebook.github.io/react-native/docs/linking.html#openurl
import Card from './Card';
import CardSection from './CardSection';
import Button from './Button';

const AlbumDetail = ({ album }) => {
  const { title, artist, thumbnail_image, image, url } = album;
  const {
    thumbnailStyle,
    headerContentStyle,
    thumbnailContainerStyle,
    headerTextStyle,
    imageStyle
  } = styles;

  return (
    <Card>
      <CardSection>
        <View style={thumbnailContainerStyle}>
          <Image
            style={thumbnailStyle}
            source={{ uri: thumbnail_image }}
          />
        </View>
        <View style={headerContentStyle}>
          <Text style={headerTextStyle}>{title}</Text>
          <Text>{artist}</Text>
        </View>
      </CardSection>

      <CardSection>
        <Image
          style={imageStyle}
          source={{ uri: image }}
        />
      </CardSection>
      
      <CardSection>
        <Button  handleOnPress={ ()=> Linking.openURL(url) }   />
      </CardSection>
    </Card>
  );
};


const styles = {
  headerContentStyle: {
    // flexDirection: 'column',
    justifyContent: 'space-around'
  },
  headerTextStyle: {
    fontSize: 18
  },
  thumbnailStyle: {
    height: 50,
    width: 50
  },
  thumbnailContainerStyle: {
    // justifyContent: 'center',
    // alignItems: 'center',
    // marginLeft: 10,
    // marginRight: 10
  },
  imageStyle: {
    height: 300,
    // flex: 1,
    // 預設是沒寬度
    // width: null
    width:'100%'
  }
};

export default AlbumDetail;
